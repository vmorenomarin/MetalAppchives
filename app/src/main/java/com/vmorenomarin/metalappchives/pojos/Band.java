package com.vmorenomarin.metalappchives.pojos;

/**
 * Created by victor on 8/29/16.
 */
public class Band {
    private int logo;
    private String name;
    private String country;
    private String location;
    private String status;
    private String formed;
    private String yearsact;    // Years active
    private String genre;
    private String themes;      //Lyrical themes
    private String llabel;      // Last Label
    private String comment;

    public Band(int logo, String name, String country, String location, String status, String formed, String yearsact, String genre, String themes, String llabel, String comment){
        this.logo       = logo;
        this.name       = name;
        this.country    = country;
        this.location   = location;
        this.status     = status;
        this.formed     = formed;
        this.yearsact   = yearsact;
        this.genre      = genre;
        this.themes     = themes;
        this.llabel     = llabel;
        this.comment    = comment;
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFormed() {
        return formed;
    }

    public void setFormed(String formed) {
        this.formed = formed;
    }

    public String getYearsact() {
        return yearsact;
    }

    public void setYearsact(String yearsact) {
        this.yearsact = yearsact;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getThemes() {
        return themes;
    }

    public void setThemes(String themes) {
        this.themes = themes;
    }

    public String getLlabel() {
        return llabel;
    }

    public void setLlabel(String llabel) {
        this.llabel = llabel;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}



package com.vmorenomarin.metalappchives.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.vmorenomarin.metalappchives.BandActivity;
import com.vmorenomarin.metalappchives.pojos.Band;
import com.vmorenomarin.metalappchives.R;

import java.util.ArrayList;

/**
 * Created by victor on 8/29/16.
 */

public class BandAdapter extends RecyclerView.Adapter<BandAdapter.BandViewHolder>{

    ArrayList<Band> stats;
    Activity activity;

    public BandAdapter(ArrayList<Band> stats, Activity activity){
        this.stats      = stats;
        this.activity   = activity;
    }

    @Override
    public BandViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_stats, parent, false);
        return new BandViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BandViewHolder bandViewHolder, int position) {
        final Band band = stats.get(position);
//        bandViewHolder.imLogoCV.setImageResource(band.getLogo());
//        bandViewHolder.tvBandName.setText(band.getName());
        bandViewHolder.tvCountry.setText(band.getCountry());
        bandViewHolder.tvLocation.setText(band.getLocation());
        bandViewHolder.tvFormed.setText(band.getFormed());
        bandViewHolder.tvYearsActive.setText(band.getYearsact());
        bandViewHolder.tvGenre.setText(band.getGenre());
        bandViewHolder.tvLyrics.setText(band.getThemes());
        bandViewHolder.tvLabel.setText(band.getLlabel());
        bandViewHolder.tvComment.setText(band.getComment());
    }
    @Override
    public int getItemCount() {
        return stats.size();
    }



    public static class BandViewHolder extends RecyclerView.ViewHolder{

        private ImageView imLogoCV;
//        private TextView tvBandName;
        private TextView tvCountry;
        private TextView tvLocation;
        private TextView tvStatus;
        private TextView tvFormed;
        private TextView tvYearsActive;
        private TextView tvGenre;
        private TextView tvLyrics;
        private TextView tvLabel;
        private TextView tvComment;


        public BandViewHolder(View itemView) {
            super(itemView);

            imLogoCV        = (ImageView) itemView.findViewById(R.id.imLogoCV);
//            tvBandName      = (TextView) itemView.findViewById(R.id.cvBandName);
            tvCountry       = (TextView) itemView.findViewById(R.id.cvCountry);
            tvLocation      = (TextView) itemView.findViewById(R.id.cvLocation);
            tvFormed        = (TextView) itemView.findViewById(R.id.cvFormed);
            tvYearsActive   = (TextView) itemView.findViewById(R.id.cvYearsActive);
            tvGenre         = (TextView) itemView.findViewById(R.id.cvGenre);
            tvLyrics        = (TextView) itemView.findViewById(R.id.cvLyrics);
            tvLabel         = (TextView) itemView.findViewById(R.id.cvLabel);
            tvComment       = (TextView) itemView.findViewById(R.id.cvComment);

        }
    }
}

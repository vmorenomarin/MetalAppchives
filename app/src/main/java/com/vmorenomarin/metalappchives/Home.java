package com.vmorenomarin.metalappchives;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;


public class Home extends AppCompatActivity implements View.OnClickListener {

    FloatingActionButton btnSearchBand;
    EditText etband;
    private Toolbar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        actionBar = (Toolbar) findViewById(R.id.actionBar);
        setSupportActionBar(actionBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle("Metal Appchives");

        FloatingActionButton btnSearchBand = (FloatingActionButton) findViewById(R.id.btnSearchBand);
        btnSearchBand.setOnClickListener(this);
        EditText etSearch = (EditText) findViewById(R.id.etSearchInput);
        etSearch.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent move) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;

            }
        });


    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, BandActivity.class);
        intent.putExtra("bandname", R.id.etSearchInput);
        startActivity(intent);
        this.finish();
    }
}






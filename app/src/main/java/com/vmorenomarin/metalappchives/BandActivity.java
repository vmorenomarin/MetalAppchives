package com.vmorenomarin.metalappchives;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.view.ViewPager;

import com.vmorenomarin.metalappchives.adapters.PageAdapter;
import com.vmorenomarin.metalappchives.fragments.BandStatsFragment;
import com.vmorenomarin.metalappchives.fragments.DiscoFragment;

import java.util.ArrayList;


public class BandActivity extends AppCompatActivity {

    private Toolbar actionBar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_band);

        actionBar = (Toolbar) findViewById(R.id.actionBar);
        setSupportActionBar(actionBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        setUpViewPager();
        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);;

        collapsingToolbar.setExpandedTitleMarginStart(10);
        collapsingToolbar.setExpandedTitleMarginBottom(80);
        collapsingToolbar.setCollapsedTitleTextColor(Color.TRANSPARENT);
        collapsingToolbar.setTitle("Windir");  //Cambiar con nombre de banda

    }

    private ArrayList<Fragment> addFragments () {
        ArrayList<Fragment> fragments = new ArrayList<>();

        fragments.add(new BandStatsFragment());
        fragments.add(new DiscoFragment());

        return fragments;
    }

    public void setUpViewPager() {
        viewPager.setAdapter(new PageAdapter(getSupportFragmentManager(), addFragments()));
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setText("Info");
        tabLayout.getTabAt(1).setText("Discography");
    }
}
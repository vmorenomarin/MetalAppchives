package com.vmorenomarin.metalappchives.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.vmorenomarin.metalappchives.R;
import com.vmorenomarin.metalappchives.adapters.BandAdapter;
import com.vmorenomarin.metalappchives.pojos.Band;

import java.util.ArrayList;


public class BandStatsFragment extends Fragment {

    private RecyclerView stats;
    ArrayList<Band> band;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_band_stats, container, false);

        stats = (RecyclerView) v.findViewById(R.id.rvStats);


        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        stats.setLayoutManager(llm);
        initStats();
        initAdapter();
        /*GridLayoutManager glm = new GridLayoutManager(this, 2);*/

        return v;
    }

    private void initStats() {
        band = new ArrayList<Band>();

        band.add(new Band(R.drawable.windir_logo, "Windir", "Norway","Songdal", "Spli-up", "1994","1994-2004","Black/Folk/Viking Metal", "History, Vikings, War, Misanthropy, Myths", "Tabu Recordings", " \"Windir\" is a word in the Sognamål " +
                "Norwegian dialect, translatable as \"warrior\". They use this dialect in most of their lyrics. Windir performed their last concert in Oslo on September 3rd 2004 in honor of Valfar. "
                +  "Enslaved, Finntroll and Mindgrinder were the guest bands. The remaining members formed Vreid."));}

    public BandAdapter adapter;
    public void initAdapter(){
        adapter = new BandAdapter(band, getActivity());
        stats.setAdapter(adapter);
    }

}
